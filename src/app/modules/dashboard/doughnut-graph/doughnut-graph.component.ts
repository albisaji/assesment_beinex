import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';
import { DOUGHNUT } from 'src/app/config/graph.config';
import { D3Service } from 'src/app/service/d3.service';

@Component({
  selector: 'app-doughnut-graph',
  templateUrl: './doughnut-graph.component.html',
  styleUrls: ['./doughnut-graph.component.css']
})
export class DoughnutGraphComponent implements OnInit {

  private margin = { top: 10, right: 30, bottom: 30, left: 40 };
  private width = 450;
  private height = 450;
  private svg: any;
  private colors: any;
  private radius = Math.min(this.width, this.height) / 2 - this.margin.left;
  data:any=DOUGHNUT;
  constructor(private d3: D3Service) {}

  ngOnInit() {
    this.createSvg();
    this.drawChart();
  }
  private createSvg(): void {
    this.svg = this.d3.d3
      .select('#donut')
      .append('svg')
      .attr('viewBox', `0 0 ${this.width} ${this.height}`)
      .append('g')
      .attr(
        'transform',
        'translate(' + this.width / 2 + ',' + this.height / 2 + ')'
      );
  }

   drawChart(): void {

    // Compute the position of each group on the pie:
    let pie = this.d3.d3
      .pie()
      .sort(null) // Do not sort group by size
      .value((d:any) => {
        return d.value;
      });
    var data_ready = pie(this.data);
    var fill_color=this.d3.d3.scaleOrdinal()
                    .domain(this.data)
                    .range(['#247885','#6c9399']);


    // The arc generator
    var arc = this.d3.d3
      .arc()
      .innerRadius(this.radius * 0.5) // This is the size of the donut hole
      .outerRadius(this.radius * 0.8)
      .cornerRadius(10)
      .padAngle(0.015)
      ;

    var div=d3.select("div.chart-container")
    .append("span")
    .attr("class","tooltip-donut")
    .style("opacity",0)
    .style('top','200PX')
    .style('left','200px')
    .style('position','absolute')
    .style('font-size','20px')
    .style('font-weight','bold');


    // Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
    let arcs=this.svg
      .selectAll('allSlices')
      .data(data_ready)
      .enter()
      .append('path')
      .attr('d', arc)
      .attr('fill', function (d:any,i:any)  {return fill_color(i)})
      .attr('stroke', 'white')
      .style('stroke-width', '18px')
      .attr("class","arc")
   
      arcs.on('mouseover', function (event:any,d:any){
        div.transition()
        .duration(50)
        .style("opacity",1)
        div.html(d.data.value)
      })
      .on('mouseout', function (d:any,i:any){
        div.transition()
        .duration(50)
        .style("opacity",0)
      });
  }

}
